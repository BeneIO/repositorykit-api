package io.bene.repository.api;

/**
 * Created by Bene on 27.12.2015.
 */
public interface Hook {

    void onHook(boolean successfullyHooked);

}