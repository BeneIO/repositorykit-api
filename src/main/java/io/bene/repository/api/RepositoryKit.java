package io.bene.repository.api;

import io.bene.repository.bukkit.BukkitHookManager;
import io.bene.repository.bungee.BungeeHookManager;

/**
 * Created by Bene on 13.12.2015.
 */
public final class RepositoryKit {

    private static ServicesManager servicesManager;
    private static HookManager hookManager;

    public static ServicesManager getServicesManager() {
        return servicesManager;
    }

    static {
        try {
            Class.forName("org.bukkit.Bukkit");
            hookManager = new BukkitHookManager();
        } catch (ClassNotFoundException e) {
            hookManager = new BungeeHookManager();
        }
    }

    public static void registerHook(String pluginName, Hook hook) {
        hookManager.registerHook(pluginName, hook);
    }

    private RepositoryKit() {}

}