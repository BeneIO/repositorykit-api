package io.bene.repository.bungee;

import io.bene.repository.api.Hook;
import io.bene.repository.api.HookManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bene on 27.12.2015.
 */
public class BungeeHookManager implements HookManager {

    private List<String> plugins = new ArrayList<>();

    @Override
    public void registerHook(String pluginName, Hook hook) {
        if (ProxyServer.getInstance().getPluginManager().getPlugins().stream().filter((p) -> p.getDescription().getName().equals(pluginName)).count() == 1) {
            if (!plugins.contains(pluginName)) {
                ProxyServer.getInstance().getConsole().sendMessage(new TextComponent("�a[*] Successfully hooked " + pluginName + "."));
                plugins.add(pluginName);
            }
            hook.onHook(true);
        } else {
            if (!plugins.contains(pluginName)) {
                ProxyServer.getInstance().getConsole().sendMessage(new TextComponent("�c[*] Could not hook " + pluginName + "."));
                plugins.add(pluginName);
            }
            hook.onHook(false);
        }
    }
}