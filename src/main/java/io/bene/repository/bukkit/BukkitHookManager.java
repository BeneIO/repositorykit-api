package io.bene.repository.bukkit;

import io.bene.repository.api.Hook;
import io.bene.repository.api.HookManager;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bene on 27.12.2015.
 */
public class BukkitHookManager implements HookManager {

    private List<String> plugins = new ArrayList<>();

    @Override
    public void registerHook(String pluginName, Hook hook) {

        if (Bukkit.getPluginManager().isPluginEnabled(pluginName)) {
            if (!plugins.contains(pluginName)) {
                Bukkit.getConsoleSender().sendMessage("�a[*] Successfully hooked " + pluginName + ".");
                plugins.add(pluginName);
            }
            hook.onHook(true);
        } else {
            if (!plugins.contains(pluginName)) {
                Bukkit.getConsoleSender().sendMessage("�c[*] Could not hook " + pluginName + ".");
                plugins.add(pluginName);
            }
            hook.onHook(false);
        }
    }
}